package com.example.tea.diary.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tea.diary.R;

public class ImportanceAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private int mImportance;

    private static String[] importanceTitles = { "Low", "Middle", "High" };

    private int[] imageTitleArray = {
            R.drawable.importance_low,
            R.drawable.importance_middle,
            R.drawable.importance_high
    };

    public ImportanceAdapter(Context context) {
        super(context, R.layout.spinner_importance, R.id.tv_spinner_title, importanceTitles);
        mContext = context;
        mImportance = R.drawable.importance_low;
    }

    public ImportanceAdapter(Context context, int importance) {
        super(context, R.layout.spinner_importance, R.id.tv_spinner_title, importanceTitles);
        mContext = context;
        mImportance = importance;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImportanceHolder importanceHolder;
        if (convertView == null) {
            importanceHolder = new ImportanceHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.spinner_importance, parent, false);
            importanceHolder.subSpinner = (TextView) convertView.findViewById(R.id.tv_spinner_title);
            importanceHolder.title = (TextView) convertView.findViewById(R.id.tv_note_title);
            importanceHolder.image = (ImageView) convertView.findViewById(R.id.iv_spinner_image);
            convertView.setTag(importanceHolder);
        } else {
            importanceHolder = (ImportanceHolder) convertView.getTag();
        }
        importanceHolder.subSpinner.setText(importanceTitles[position]);
        importanceHolder.image.setImageResource(imageTitleArray[position]);
        return convertView;
    }

    private Drawable getSpinnetImageDrawable() {
        switch (mImportance) {
            case R.drawable.importance_low:
                return ContextCompat.getDrawable(getContext(), R.drawable.importance_low);
            case R.drawable.importance_middle:
                return ContextCompat.getDrawable(getContext(), R.drawable.importance_middle);
            case R.drawable.importance_high:
                return ContextCompat.getDrawable(getContext(), R.drawable.importance_high);
            default:
                return ContextCompat.getDrawable(getContext(), R.drawable.importance_low);
        }
    }

//    private int getSpinnerImageResId() {
//        switch (mImportance) {
//            case "Low":
//                return R.drawable.importance_low;
//            case "Middle":
//                return R.drawable.importance_middle;
//            case "High":
//                return R.drawable.importance_high;
//            default:
//                return R.drawable.importance_low;
//        }
//    }

    private class ImportanceHolder {
        public TextView subSpinner;
        public TextView title;
        public ImageView image;
    }
}
