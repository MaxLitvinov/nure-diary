package com.example.tea.diary.classes;

public class NoteValue {
    public static final String IMAGE = "image";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String IMPORTANCE = "importance";
    public static final String CREATED_TIME = "createdTime";
    public static final String APPOINTED_DATE = "appointedDate";
    public static final String APPOINTED_TIME = "appointedTime";
}
