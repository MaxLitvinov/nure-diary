package com.example.tea.diary.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.tea.diary.R;
import com.example.tea.diary.adapters.ImportanceAdapter;
import com.example.tea.diary.classes.Importance;
import com.example.tea.diary.classes.NoteValue;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EditNoteActivity extends AppCompatActivity {
    private final int PICK_CODE_IMAGE = 1;

    private ImageView mNoteImage;
    private EditText mNoteTitle;
    private EditText mNoteDescription;
    private Spinner mNoteImportance;
    private Button mSetDate;
    private Button mSetTime;
    private Button mSaveNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent incomingNoteIntent = getIntent();
        final long noteId = incomingNoteIntent.getLongExtra("id", -1);
        byte[] image = incomingNoteIntent.getByteArrayExtra(NoteValue.IMAGE);
        String title = incomingNoteIntent.getStringExtra(NoteValue.TITLE);
        String description = incomingNoteIntent.getStringExtra(NoteValue.DESCRIPTION);
        int importance = incomingNoteIntent.getIntExtra(NoteValue.IMPORTANCE, R.drawable.importance_low);
        String appointedDate = incomingNoteIntent.getStringExtra(NoteValue.APPOINTED_DATE);
        String appointedTime = incomingNoteIntent.getStringExtra(NoteValue.APPOINTED_TIME);

        SimpleDateFormat dateFormatPattern = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        final String createdDateAndTime =dateFormatPattern.format(new Date());

        mNoteImage = (ImageView) findViewById(R.id.iv_note_image);
        mNoteTitle = (EditText) findViewById(R.id.et_note_title);
        mNoteDescription = (EditText) findViewById(R.id.et_note_description);
        mNoteImportance = (Spinner) findViewById(R.id.spn_importance);
        mSetDate = (Button) findViewById(R.id.btn_set_date);
        mSetTime = (Button) findViewById(R.id.btn_set_time);
        mSaveNote = (Button) findViewById(R.id.btn_save_note);

        mNoteImage.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
        mNoteTitle.setText(title);
        mNoteDescription.setText(description);
        mSetDate.setText(appointedDate);
        mSetTime.setText(appointedTime);

        mNoteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getImageIntent = new Intent(Intent.ACTION_PICK);
                getImageIntent.setType("image/*");
                startActivityForResult(getImageIntent, PICK_CODE_IMAGE);
            }
        });

        mNoteImportance.setAdapter(new ImportanceAdapter(this));
        mNoteImportance.setPromptId(importance);

        mSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new DatePickerFragment();
                dateFragment.show(getSupportFragmentManager(), "Choose date");
            }
        });

        mSetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timeFragment = new TimePickerFragment();
                timeFragment.show(getSupportFragmentManager(), "Choose time");
            }
        });

        mSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] image = convertImageToByte(mNoteImage);
                String title = mNoteTitle.getText().toString();
                String description = mNoteDescription.getText().toString();
                String importance = mNoteImportance.getSelectedItem().toString();
                String appointedDate = mSetDate.getText().toString();
                String appointedTime = mSetTime.getText().toString();

                Intent backData = new Intent();
                backData.putExtra("id", noteId);
                backData.putExtra(NoteValue.IMAGE, image);
                backData.putExtra(NoteValue.TITLE, title);
                backData.putExtra(NoteValue.DESCRIPTION, description);
                backData.putExtra(NoteValue.IMPORTANCE, importance);
                backData.putExtra(NoteValue.APPOINTED_DATE, appointedDate);
                backData.putExtra(NoteValue.APPOINTED_TIME, appointedTime);
                backData.putExtra(NoteValue.CREATED_TIME, createdDateAndTime);

                setResult(RESULT_OK, backData);
                finish();
            }
        });
    }

    private byte[] convertImageToByte(ImageView imageView) {
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private Importance getImportanceFromString(String importance) {
        switch (importance) {
            case "Low":
                return Importance.LOW;
            case "Middle":
                return Importance.MIDDLE;
            case "High":
                return Importance.HIGH;
            default:
                return Importance.LOW;
        }
    }
}
