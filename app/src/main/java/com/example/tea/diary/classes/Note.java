package com.example.tea.diary.classes;

public class Note {
    private String mTitle;
    private String mDescription;
    private Importance mImportance;
    private String mCreatedTime;
    private String mAppointedTime;
    private String mAppointedDate;
    private byte[] mImage;

    public Note() {}

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getImportance() {
        return mImportance.getmImage();
    }

    public void setImportance(Importance mImportance) {
        this.mImportance = mImportance;
    }

    public String getCreatedTime() {
        return mCreatedTime;
    }

    public void setCreatedTime(String mCreatedTime) {
        this.mCreatedTime = mCreatedTime;
    }

    public String getAppointedTime() {
        return mAppointedTime;
    }

    public void setAppointedTime(String mAppointedTime) {
        this.mAppointedTime = mAppointedTime;
    }

    public String getAppointedDate() {
        return mAppointedDate;
    }

    public void setAppointedDate(String mAppointedDate) {
        this.mAppointedDate = mAppointedDate;
    }

    public byte[] getImage() {
        return mImage;
    }

    public void setImage(byte[] mImage) {
        this.mImage = mImage;
    }
}
