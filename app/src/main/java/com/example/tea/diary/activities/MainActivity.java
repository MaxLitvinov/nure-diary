package com.example.tea.diary.activities;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.tea.diary.R;
import com.example.tea.diary.adapters.NoteAdapter;
import com.example.tea.diary.classes.Importance;
import com.example.tea.diary.classes.Note;
import com.example.tea.diary.classes.NoteValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private static final int REQUESTED_CODE_ADD_NOTE = 1;
    private static final int REQUESTED_CODE_EDIT_NOTE = 2;

    private List<Note> mNotesList;
    private NoteAdapter mNoteAdapter;
    private ListView mNotesListView;

    public static class SearchOption {
        public static boolean SEARCH_DESCRIPTION = false;
        public static boolean FILTER_DESCRIPTION = false;
        public static boolean FILTER_IMPORTANCE = false;
    }

    private Locale locale = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources resources = getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        configuration.locale = Locale.ROOT;
        Resources locRes = new Resources(resources.getAssets(), resources.getDisplayMetrics(), configuration);

        String appName = locRes.getString(R.string.app_name);
        setTitle(appName);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mNotesList = new ArrayList<>();

        mNoteAdapter = new NoteAdapter(this, mNotesList);

        mNotesListView = (ListView) findViewById(R.id.lv_notes_list);
        mNoteAdapter.notifyDataSetChanged();
        mNotesListView.setAdapter(mNoteAdapter);

        registerForContextMenu(mNotesListView);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale == null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                if (SearchOption.SEARCH_DESCRIPTION) {
                    mNoteAdapter.getFilter().filter(newText);
                } else if (SearchOption.FILTER_DESCRIPTION) {
                    mNoteAdapter.getFilter().filter(newText);
                } else if (SearchOption.FILTER_IMPORTANCE) {

                }
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_note:
                Intent addNoteIntent = new Intent(this, AddNoteActivity.class);
                startActivityForResult(addNoteIntent, REQUESTED_CODE_ADD_NOTE);
                return true;
            case R.id.note_search_desctiption_option:
                SearchOption.SEARCH_DESCRIPTION = true;
                SearchOption.FILTER_DESCRIPTION = false;
                SearchOption.FILTER_IMPORTANCE = false;
                item.setChecked(true);
                return true;
            case R.id.note_filter_description_option:
                SearchOption.SEARCH_DESCRIPTION = false;
                SearchOption.FILTER_DESCRIPTION = true;
                SearchOption.FILTER_IMPORTANCE = false;
                item.setChecked(true);
                return true;
            case R.id.note_importance_option:
                SearchOption.SEARCH_DESCRIPTION = false;
                SearchOption.FILTER_DESCRIPTION = false;
                SearchOption.FILTER_IMPORTANCE = true;
                item.setChecked(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(android.R.drawable.arrow_down_float);
        menu.setHeaderTitle(R.string.choose_action);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit:
                long noteId = info.id;
                byte[] image = mNotesList.get((int) noteId).getImage();
                String title = mNotesList.get((int) noteId).getTitle();
                String description = mNotesList.get((int) noteId).getDescription();
                int importance = mNotesList.get((int) noteId).getImportance();
                String createdTime = mNotesList.get((int) noteId).getCreatedTime();
                String appointedDate = mNotesList.get((int) noteId).getAppointedDate();
                String appointedTime = mNotesList.get((int) noteId).getAppointedTime();

                Intent editIntent = new Intent(this, EditNoteActivity.class);
                editIntent.putExtra("id", noteId);
                editIntent.putExtra(NoteValue.IMAGE, image);
                editIntent.putExtra(NoteValue.TITLE, title);
                editIntent.putExtra(NoteValue.DESCRIPTION, description);
                editIntent.putExtra(NoteValue.IMPORTANCE, importance);
                editIntent.putExtra(NoteValue.CREATED_TIME, createdTime);
                editIntent.putExtra(NoteValue.APPOINTED_DATE, appointedDate);
                editIntent.putExtra(NoteValue.APPOINTED_TIME, appointedTime);
                startActivityForResult(editIntent, REQUESTED_CODE_EDIT_NOTE);
                return true;
            case R.id.delete:
                showDeleteDialog(info.id);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showDeleteDialog(final long id) {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle(R.string.deleting);
        deleteDialog.setMessage(R.string.are_you_sure_want_to_delete_this_note);
        deleteDialog.setIcon(android.R.drawable.ic_menu_delete);

        deleteDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mNotesList.remove((int) id);
                mNoteAdapter.notifyDataSetChanged();
            }
        });

        deleteDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        deleteDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUESTED_CODE_ADD_NOTE) {
                byte[] image = data.getByteArrayExtra(NoteValue.IMAGE);
                String title = data.getStringExtra(NoteValue.TITLE);
                String description = data.getStringExtra(NoteValue.DESCRIPTION);
                String importance = data.getStringExtra(NoteValue.IMPORTANCE);
                String createdTime = data.getStringExtra(NoteValue.CREATED_TIME);
                String appointedTime = data.getStringExtra(NoteValue.APPOINTED_DATE);
                String appointedDate = data.getStringExtra(NoteValue.APPOINTED_TIME);

                Note note = new Note();
                note.setImage(image);
                note.setTitle(title);
                note.setDescription(description);
                note.setImportance(getImportanceFromString(importance));
                note.setCreatedTime(createdTime);
                note.setAppointedDate(appointedDate);
                note.setAppointedTime(appointedTime);

                mNotesList.add(note);
                mNoteAdapter = new NoteAdapter(this, mNotesList);
                mNotesListView.setAdapter(mNoteAdapter);
            }
        }
    }

    private Importance getImportanceFromString(String importance) {
        switch (importance) {
            case "Low":
                return Importance.LOW;
            case "Middle":
                return Importance.MIDDLE;
            case "High":
                return Importance.HIGH;
            default:
                return Importance.LOW;
        }
    }
}
