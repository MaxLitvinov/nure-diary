package com.example.tea.diary.classes;

import com.example.tea.diary.R;

public enum Importance {
    LOW (1, R.drawable.importance_low),
    MIDDLE (2, R.drawable.importance_middle),
    HIGH (3, R.drawable.importance_high);

    private int mValue;
    private int mImage;

    Importance(int value, int image) {
        mValue = value;
        mImage = image;
    }

    int getmImage() {
        return mImage;
    }
}
