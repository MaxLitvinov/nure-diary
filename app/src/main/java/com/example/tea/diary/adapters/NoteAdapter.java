package com.example.tea.diary.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tea.diary.R;
import com.example.tea.diary.activities.MainActivity;
import com.example.tea.diary.classes.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {
    private List<Note> mNotesList;
    private Filter filter;

    public NoteAdapter(Context context, List<Note> notesList) {
        super(context, R.layout.note_short_view, notesList);
        mNotesList = notesList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note note = getItem(position);
        byte[] image = note.getImage();
        NoteHolder noteHolder;
        if (convertView == null) {
            noteHolder = new NoteHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.note_short_view, parent, false);
            noteHolder.noteImage = (ImageView) convertView.findViewById(R.id.iv_note_image);
            noteHolder.noteTitle = (TextView) convertView.findViewById(R.id.tv_note_title);
            noteHolder.noteCreationTime = (TextView) convertView.findViewById(R.id.tv_note_creation_time);
            noteHolder.noteImportance = (ImageView) convertView.findViewById(R.id.iv_note_importance);
            convertView.setTag(noteHolder);
        } else {
            noteHolder = (NoteHolder) convertView.getTag();
        }
        noteHolder.noteImage.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
        noteHolder.noteTitle.setText(note.getTitle());
        noteHolder.noteCreationTime.setText(note.getCreatedTime());
        noteHolder.noteImportance.setImageResource(note.getImportance());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            if (MainActivity.SearchOption.SEARCH_DESCRIPTION) {
                filter = new NoteSearchByDescription(mNotesList);
            } else if (MainActivity.SearchOption.FILTER_DESCRIPTION) {
                filter = new NoteFilterByDescription(mNotesList);
            } else if (MainActivity.SearchOption.FILTER_IMPORTANCE) {
                filter = new NoteFilterByImportance(mNotesList);
            }
        }
        return filter;
    }

    private class NoteHolder {
        public ImageView noteImage;
        public TextView noteTitle;
        public TextView noteCreationTime;
        public ImageView noteImportance;
    }

//    private class SearchFilter extends Filter {
//        private List<Note> sourceList;
//
//        public SearchFilter(List<Note> list) {
//            sourceList = new ArrayList<>();
//            synchronized (this) {
//                sourceList.addAll(list);
//            }
//        }
//
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            String filterString = constraint.toString().toLowerCase();
//            FilterResults result = new FilterResults();
//            if (filterString.length() != 0) {
//                List<Note> filteredList = new ArrayList<>();
//
//                for (Note notes : sourceList) {
//                    if (notes.getTitle().toLowerCase().contains(filterString)) {
//                        filteredList.add(notes);
//                    }
//                }
//                result.values = filteredList;
//                result.count = filteredList.size();
//            } else {
//                synchronized (this) {
//                    result.values = sourceList;
//                    result.count = sourceList.size();
//                }
//            }
//            return result;
//        }
//
//        @SuppressWarnings("unchecked")
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            List<Note> filteredList = (List<Note>) results.values;
//            notifyDataSetChanged();
//            clear();
//            for (int i = 0; i < filteredList.size(); i++) {
//                add(filteredList.get(i));
//            }
//            notifyDataSetChanged();
//        }
//    }


    private class NoteSearchByDescription extends Filter {
        private List<Note> sourceList;

        public NoteSearchByDescription(List<Note> list) {
            sourceList = new ArrayList<>();
            synchronized (this) {
                sourceList.addAll(list);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterString.length() != 0) {
                List<Note> filteredList = new ArrayList<>();

                for (Note notes : sourceList) {
                    if (notes.getDescription().toLowerCase().equals(filterString)) {
                        filteredList.add(notes);
                    }
                }
                result.values = filteredList;
                result.count = filteredList.size();
            } else {
                synchronized (this) {
                    result.values = sourceList;
                    result.count = sourceList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Note> filteredList = (List<Note>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0; i < filteredList.size(); i++) {
                add(filteredList.get(i));
            }
            notifyDataSetInvalidated();
        }
    }

    private class NoteFilterByDescription extends Filter {
        private List<Note> sourceList;

        public NoteFilterByDescription(List<Note> list) {
            sourceList = new ArrayList<>();
            synchronized (this) {
                sourceList.addAll(list);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterString.length() != 0) {
                List<Note> filteredList = new ArrayList<>();

                for (Note notes : sourceList) {
                    if (notes.getDescription().toLowerCase().contains(filterString)) {
                        filteredList.add(notes);
                    }
                }
                result.values = filteredList;
                result.count = filteredList.size();
            } else {
                synchronized (this) {
                    result.values = sourceList;
                    result.count = sourceList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Note> filteredList = (List<Note>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0; i < filteredList.size(); i++) {
                add(filteredList.get(i));
            }
            notifyDataSetInvalidated    ();
        }
    }

    private class NoteFilterByImportance extends Filter {
        private List<Note> sourceList;

        public NoteFilterByImportance(List<Note> list) {
            sourceList = new ArrayList<>();
            synchronized (this) {
                sourceList.addAll(list);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterString.length() != 0) {
                List<Note> filteredList = new ArrayList<>();

                for (Note notes : sourceList) {
                    if (notes.getTitle().toLowerCase().contains(filterString)) {
                        filteredList.add(notes);
                    }
                }
                result.values = filteredList;
                result.count = filteredList.size();
            } else {
                synchronized (this) {
                    result.values = sourceList;
                    result.count = sourceList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Note> filteredList = (List<Note>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0; i < filteredList.size(); i++) {
                add(filteredList.get(i));
            }
            notifyDataSetInvalidated();
        }
    }
}
